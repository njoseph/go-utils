# Go Utils

Utilities written in Go language, mostly to make use of cheap concurrency using goroutines.

## Usage

Compile and put goutils binary somewhere on your PATH.

## Utils

### Git Pull All

Command to pull all the repositories under a given directory.

```shell script
cd dir/with/all/the/repos
goutils git-pull-all

# Alias
goutils gpa
```

## Aliases

```shell script
alias gpa="goutils git-pull-all"
```

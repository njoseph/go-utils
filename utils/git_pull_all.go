package utils

import (
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"sync"
)

// Equivalent to the following shell alias
// alias git-pull-all="ls -d */ | xargs -P12 -I{} git -C {} pull"

func GitPullAll() {
	currentDirectory, err := os.Getwd()
	if err != nil {
		log.Fatal("Error when trying to get current directory!")
	} else {
		log.Println("Pulling all git repositories in the directory: ", currentDirectory, "...")

		files, err := ioutil.ReadDir("./")
		if err != nil {
			log.Fatal(err.Error())
		}
		var wg sync.WaitGroup

		for _, f := range files {
			if f.IsDir() {
				wg.Add(1)
				go doGitPull(&wg, f)
			}
		}
		wg.Wait()
	}
}

func doGitPull(wg *sync.WaitGroup, dir os.FileInfo) {
	defer wg.Done()
	output, err := exec.Command("git", "-C", dir.Name(), "pull", "--rebase").Output()
	if err != nil {
		log.Println("Error pulling", dir.Name(), err.Error())
	}
	log.Println("Pulling", dir.Name(), "\n", string(output))
}

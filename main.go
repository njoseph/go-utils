package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"njoseph.me/goutils/utils"
)

var app = cli.NewApp()

func Info() {
	app.Name = "Go Utils"
	app.Usage = "goutils command"
	app.Author = "Joseph Nuthalapati"
	app.Version = "0.0.1"
}

func Commands() {
	app.Commands = []cli.Command{
		{
			Name:    "git-pull-all",
			Aliases: []string{"gpa"},
			Action: func(c *cli.Context) {
				utils.GitPullAll()
			},
		},
	}
}

func main() {
	Info()
	Commands()

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
